/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.dao.interfaces;

import java.util.List;

/**
 *
 * @author Dagdoug
 */
public interface IcrudDAO<T> {
    
    void add(T t);    
    void update(T t);    
    void delete(int id);    
    List<T> displayAll();
    T findById(int id);
    
}
