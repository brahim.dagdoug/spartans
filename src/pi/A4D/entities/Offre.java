/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.entities;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author Dagdoug
 */
public class Offre {

    private int idOffre;
    private String libelle;
    private String description;
    private String adresse ;
    private String ville ;
    private String gouvernat ;
    private int nombrePoints ;
    private boolean validation ;
    private Date datePost ;
    private Categorie categorie;
    private List<Avis> avis;

    public Offre() {
    }

    public Offre(int idOffre, String libelle, String description, String adresse, String ville, String gouvernat, int nombrePoints, boolean validation, Date datePost) {
        this.idOffre = idOffre;
        this.libelle = libelle;
        this.description = description;
        this.adresse = adresse;
        this.ville = ville;
        this.gouvernat = gouvernat;
        this.nombrePoints = nombrePoints;
        this.validation = validation;
        this.datePost = datePost;
    }

    public Offre(String libelle, String description, String adresse, String ville, String gouvernat, int nombrePoints, boolean validation, Date datePost) {
        this.libelle = libelle;
        this.description = description;
        this.adresse = adresse;
        this.ville = ville;
        this.gouvernat = gouvernat;
        this.nombrePoints = nombrePoints;
        this.validation = validation;
        this.datePost = datePost;
    }

    public int getIdOffre() {
        return idOffre;
    }

    public void setIdOffre(int idOffre) {
        this.idOffre = idOffre;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getGouvernat() {
        return gouvernat;
    }

    public void setGouvernat(String gouvernat) {
        this.gouvernat = gouvernat;
    }

    public int getNombrePoints() {
        return nombrePoints;
    }

    public void setNombrePoints(int nombrePoints) {
        this.nombrePoints = nombrePoints;
    }

    public boolean isValidation() {
        return validation;
    }

    public void setValidation(boolean validation) {
        this.validation = validation;
    }

    public Date getDatePost() {
        return datePost;
    }

    public void setDatePost(Date datePost) {
        this.datePost = datePost;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public List<Avis> getAvis() {
        return avis;
    }

    public void setAvis(List<Avis> avis) {
        this.avis = avis;
    }

    @Override
    public String toString() {
        return "Offre{" + "idOffre=" + idOffre + ", libelle=" + libelle + ", description=" + description + ", adresse=" + adresse + ", ville=" + ville + ", gouvernat=" + gouvernat + ", nombrePoints=" + nombrePoints + ", validation=" + validation + ", datePost=" + datePost + '}';
    }

    

}
