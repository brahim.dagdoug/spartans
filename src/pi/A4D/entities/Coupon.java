/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.entities;

/**
 *
 * @author Dagdoug
 */
public class Coupon {

    private int id;
    private Service service;

    public Coupon() {
    }

    public Coupon(Service service) {
        this.service = service;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

}
