/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.entities;

import java.util.List;

/**
 *
 * @author Dagdoug
 */
public class Panier {
    private int id;
    private int qte ;
    private Produit produit;
    private Membre membre;

    public Panier() {
    }
   
    public Panier(int id, int qte,Produit produit, Membre membre) {
        this.id = id;
        this.qte = qte;
        this.produit = produit;
        this.membre = membre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produits) {
        this.produit = produit;
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }
    

  
}
